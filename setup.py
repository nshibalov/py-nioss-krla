# pylint: disable-msg=invalid-name
# coding: utf-8
import uuid
from cx_Freeze import setup, Executable

proj_name = "nioss_parse"

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    "packages": ["os", "decimal"],
    "excludes": ["tkinter", "PySide", "PIL", "numpy", "pandas"],
    "include_files": [
        "nioss_db.xlsx"
    ]
}


bdist_msi_options = {
    "upgrade_code": "{{{}}}".format(
        uuid.uuid5(uuid.NAMESPACE_DNS, proj_name))
}


setup(
    name=proj_name,
    version="0.1",
    description=proj_name,
    author="Shibalov N. A.",
    author_email="nshibalov@gmail.com",
    options={
        "build_exe": build_exe_options,
        "bdist_msi": bdist_msi_options
        },
    executables=[
        Executable(
            "nioss.py",
            shortcutName=proj_name,
            shortcutDir="ProgramMenuFolder"),
        Executable(
            "update_db.py",
            shortcutName=proj_name,
            shortcutDir="ProgramMenuFolder"),
        Executable(
            "update_db_all.py",
            shortcutName=proj_name,
            shortcutDir="ProgramMenuFolder"),
        ])
