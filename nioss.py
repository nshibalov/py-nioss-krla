# coding=utf-8

import os
import re
import sys
import openpyxl


HW_RRS = "RRS"


def get_exe_fname():
    return os.path.abspath(sys.argv[0])


def get_exe_path():
    return os.path.dirname(get_exe_fname())


def is_int(_val):
    try:
        int(_val)
        return True
    except (ValueError, TypeError):
        pass
    return False


def to_int(_str):
    try:
        return int(_str)
    except (ValueError, TypeError):
        pass
    return None


def list_to_ws(_list, _ws, _row, _col=0):
    for col, value in enumerate(_list, max(_col, 1)):
        _ws.cell(column=col, row=_row, value=value)


def parse_nioss_db():
    ret = []
    try:
        db_fname = os.path.join(get_exe_path(), "nioss_db.xlsx")
        print("Файл БД: '{}'".format(db_fname))
        wbook = openpyxl.load_workbook(
            filename=db_fname,
            read_only=True
        )
        try:
            wsheet = wbook["Nioss DB"]
        except (KeyError, UnicodeEncodeError):
            wsheet = wbook.active

        max_row = wsheet.max_row
        print("Разбираем БД...")
        for i, row in enumerate(wsheet.iter_rows(min_row=2)):
            if row[2].value != HW_RRS:
                continue
            oid = row[0].value
            name = row[1].value
            ret.append((oid, name))

            if not i % 1000:
                print("{: >7.2%}\r".format(float(i) / max_row), end="")

    except KeyError as err:
        print(err)
    except openpyxl.utils.exceptions.InvalidFileException as err:
        print(err)
    finally:
        print()
        print("Готово.")
    return ret


def parse_nioss(_fname):
    ret = {}
    try:
        wbook = openpyxl.load_workbook(
            filename=_fname, data_only=True, read_only=True)
        for wsheet in wbook.worksheets:
            pairs = []
            for row in wsheet.iter_rows():
                site_from = to_int(row[0].value)
                if site_from is None:
                    continue

                site_to = to_int(row[1].value)
                if site_to is None:
                    continue

                pairs.append((site_from, site_to))
            ret[wsheet.title] = pairs
    except KeyError as err:
        print(err)
    except openpyxl.utils.exceptions.InvalidFileException as err:
        print(err)
    return ret


def save_pairs(_wsheet, _pair_to_nid, _pairs):
    try:
        clean_name = re.compile(r"RRS_\d+_\d+<>\d+_\d+$")

        ret = []
        max_nid_len = 0
        for pair in _pairs:
            source, target = pair

            nids = [("N/F", None)]
            if pair in _pair_to_nid:
                nids = sorted(
                    _pair_to_nid[pair],
                    key=lambda x: -1 if clean_name.match(x[0]) else x[1]
                )

            if len(nids) > max_nid_len:
                max_nid_len = len(nids)

            ret.append((source, target, nids))

        header = ["From", "To"]
        header += ["ID{}".format(x) for x in range(1, max_nid_len + 1)]
        header += ["NAME{}".format(x) for x in range(1, max_nid_len + 1)]
        list_to_ws(header, _wsheet, 1)
        for row, (source, target, nids) in enumerate(ret, 2):
            lst = [source, target]
            lst += [
                "https://nioss/ncobject.jsp?id={}".format(
                    nid) if nid is not None else "-"
                for _, nid in nids
            ]
            lst += [""] * (max_nid_len - len(nids))
            lst += [name for name, _ in nids]
            list_to_ws(lst, _wsheet, row)
    except IOError as err:
        print(err)


def nios_rss_to_xml(_ifname):
    print("Файл: '{}'".format(_ifname))

    base_path, ext = os.path.splitext(os.path.abspath(_ifname))
    ofname = "{}_res{}".format(base_path, ext)

    pairs_by_ws_title = parse_nioss(_ifname)

    try:
        pair_to_nid = {}
        nioss_db = parse_nioss_db()
        for nid, name in nioss_db:
            match = re.match(r"RRS_77_(\d+)<>77_(\d+)(.*)", name)
            if match is None:
                continue

            source = int(match.group(1))
            target = int(match.group(2))

            pair = (source, target)

            if pair not in pair_to_nid:
                pair_to_nid[pair] = []

            pair_to_nid[pair].append((name.upper(), int(nid)))

        wbook = openpyxl.Workbook()
        wbook.remove(wbook.active)

        for wsheet_title, pairs in pairs_by_ws_title.items():
            save_pairs(
                wbook.create_sheet(title=wsheet_title),
                pair_to_nid,
                pairs)

        wbook.save(ofname)
    except IOError as err:
        print(err)


def main():
    if len(sys.argv) < 2:
        print("Пожалуйста укажите файл")
        sys.exit(1)
    nios_rss_to_xml(sys.argv[1])


if __name__ == "__main__":
    main()
