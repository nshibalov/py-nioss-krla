# coding=utf-8

import MySQLdb
import openpyxl


class JournalDB:
    SERVER = "10.233.208.152"
    PORT = 3306
    LOGIN = "engineer"
    PASSWORD = "phee0Wie!"
    DATABASE = "oss"

    def __init__(self):
        pass

    def iter_nios_rrs(self):
        try:
            db = MySQLdb.connect(
                host=self.SERVER,
                port=self.PORT,
                user=self.LOGIN,
                passwd=self.PASSWORD,
                db=self.DATABASE,
                charset="utf8"
            )

            cur = db.cursor()

            cur.execute(
                "SELECT external_id "
                "    , name "
                "    , rm.label "
                "    , rs.id "
                "    , rs.label "
                "FROM "
                "  remedy_network_element "
                "JOIN "
                "  remedy_menu AS rm "
                "ON "
                "  rm.id = hwid_id "
                "JOIN "
                "  remedy_select AS rs "
                "ON "
                "  rs.id = status_id "
                "WHERE "
                "hwid_id = 7188;"
            )

            for nioss_id, obj_name, hw_id, status_id, status_name in cur:
                yield (nioss_id, obj_name, hw_id, status_id, status_name)

        except Exception as err:
            print(err)
        finally:
            if db is not None:
                db.close()


def list_to_ws(_list, _ws, _row, _col=0):
    for col, value in enumerate(_list, max(_col, 1)):
        _ws.cell(column=col, row=_row, value=value)


if __name__ == "__main__":
    print("Updating DB...")
    jounral_db = JournalDB()

    wbook = openpyxl.Workbook()
    nios_db_ws = wbook.create_sheet(title=u"Nioss DB")
    list_to_ws(
        [u"ID", u"NAME", u"HW_TYPE_ID", u"STATUS ID", u"STATUS"],
        nios_db_ws,
        1
    )
    for row, data in enumerate(jounral_db.iter_nios_rrs(), 2):
        list_to_ws(data, nios_db_ws, row)
    wbook.remove(wbook.active)
    wbook.save("nioss_db.xlsx")
